(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === "object") {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.domready = factory();
    }
}(this, function () {

    var domready = function( fn )
    {
        // Sanity check
        if ( typeof fn !== "function" ) {
            return;
        }

        // If document is already loaded, run method
        // (readystate is either interactive or complete)
        if ( document.readyState !== "loading" ) {
            return fn();
        }

        // Otherwise, wait until document is loaded
        document.addEventListener( "DOMContentLoaded", fn, false );
    };


    return domready;
}));
