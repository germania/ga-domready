#ga-domready

Convenient domready wrapper: Although native support of `document.addEventListener('DOMContentLoaded')` should be fine since IE9, using this module might be easier to write and read.

See [Caniuse DOMContentLoaded](http://caniuse.com/#search=DOMContentLoaded)


##Installation

```bash
npm install ga-domready --save
```

##Usage

```js
// Require module
var domready = require('ga-domready');

// Use like this
domready( function() {
	// noop
});
```

##Develop with Gulp

Use [Git Flow](https://github.com/nvie/gitflow), always work in `develop` branch.

- Install development dependencies: `npm install`
- Run `gulp watch`
- Work in `src/`

Builds are now in `dist/`